import csv
import datetime
import json

import mysql.connector
from mysql.connector import Error

cnx = mysql.connector.connect(host='localhost',
                              database='cja',
                              user='root',
                              password='root')
cursor = cnx.cursor()

query = ("SELECT Page.SessionKey, Page.Title FROM Page")

cursor.execute(query)

with open('session_dataset.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Session', 'Title'])
    for row in cursor.fetchmany(550000):
        sessionkey = row[0]
        title = row[1]
        writer.writerow([sessionkey, title])
cursor.close()
cnx.close()
