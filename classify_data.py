import csv
import os
import pdb
from collections import OrderedDict

import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)


def contains(a_list, a_sentence):
    for item in a_list:
        if item.lower() in a_sentence.lower():
            return True
    return False


class Classify(object):

    def __init__(self, title=None):
        self.title = title
        self.category = OrderedDict([(
            "HIGHCAREER", ['Current Job Opening', 'Submit Resume',
                           'Getting Hired', 'Thanks for', 'Contacting', 'Getting in Touch']),
            ("HIGHSERVICE", ['Hire', 'Developers', 'Remote', 'Dedicated',
                             'Thank you', 'Enquiry', 'contact Net solutions']),
            ("MEDIUMCAREER", ['Career Opportunity', 'Jobs',
                              'Opportunities for your', 'Career Growth']),
            ("MEDIUMSERVICE", ['Services and Solutions',
                               'Development Services', 'insights']),
            ("LOWCAREER", ['Jobs', 'Career'])
        ])

        self.category_weightage = {
            'HIGHCAREER': 7,
            'HIGHSERVICE': 5,
            'MEDIUMCAREER': 0,
            'MEDIUMSERVICE': 0,
            'LOWCAREER': 0,
            'OTHERS': 0,
        }
        self.categorize = {
            'CAREER': 7,
            'SERVICE': 5,
            'OTHERS': 0,
            'BOTH': 12,
        }

    def find_weightage(self, key):
        return self.category_weightage[key]

    def find_category(self, value_list):
        # for name, age in dictionary.iteritems():  (for Python 2.x)
        for key, value in self.categorize.items():
            if value == value_list:
                return key

    def classify(self):
        """Returns a json structure with classification for sentences within the text

        c.classify("The nurse will be there. We'll be there around 10:30")
        {'medpro': ['The nurse will be there.'],
            'scheduling': ["We'll be there around 10:30"]}

        """
        for key in self.category.keys():
            if contains(self.category[key], self.title):
                return key
        return 'OTHERS'


with open('session_dataset.csv', 'r') as csvinput:
    with open('dataset_classified.csv', 'w') as csvoutput:
        writer = csv.writer(csvoutput)

        for row in csv.reader(csvinput):
            if row[0] == "Session":
                writer.writerow(["Session", "Title", "Category"])
            else:
                cat = Classify(row[1]).classify()
                writer.writerow([row[0], row[1], cat])
dataset = pd.read_csv('dataset_classified.csv')
dataset.head(100)
dataset = dataset.groupby(['Session', 'Category']
                          ).size().reset_index(name='count')
with open('dataset_weightage.csv', 'w') as csvoutput:
    writer = csv.writer(csvoutput)
    writer.writerow(["Session", "Category", "count", "Weight"])
    for index, row in dataset.iterrows():
        weight = Classify().find_weightage(row["Category"])
        writer.writerow(
            [row["Session"], row["Category"], row['count'], weight])

weighted_dataset = pd.read_csv('dataset_weightage.csv')
weighted_dataset = weighted_dataset.drop(['count', 'Category'], axis=1)
weighted_dataset = weighted_dataset.groupby('Session').sum()
with open('final_session_classification.csv', 'w') as csvoutput:
    writer = csv.writer(csvoutput)
    writer.writerow(["Session", "Category"])
    for index, row in weighted_dataset.iterrows():
        cat = Classify().find_category(int(row["Weight"]))
        writer.writerow([row.name, cat])
