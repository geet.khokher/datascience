import os
import pdb

import matplotlib as mlp
import matplotlib.pyplot as plt
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import plotly.graph_objs as go
import plotly.plotly as py
import seaborn as sns
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot, plot

init_notebook_mode(connected=True)

session_info = pd.read_csv('session_info_dataset.csv')
final_session_classification = pd.read_csv('final_session_classification.csv')
result = pd.merge(session_info, final_session_classification, on='Session')
result.to_csv('final_result.csv', index=False)


plt.figure(figsize=(25, 30))
result['Monthly'] = result.apply(lambda x: '%s_%s' %
                                 (x['DateMonth'], x['DateYear']), axis=1)
descending_order = result['Country'].value_counts(
).sort_values(ascending=False).index
sns_plot = sns.countplot(x=result['Category'], hue=result['Monthly'],
                         linewidth=4)
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=50, fontsize=5, ha="right")
fig = sns_plot.get_figure()
fig.savefig("yearly.png")
plt.title('Yearly')
plt.show()


plt.figure(figsize=(20, 15))
descending_order = result['Category'].value_counts(
).sort_values(ascending=False).index
sns_plot = sns.countplot(
    x=result['Category'], linewidth=4)
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=40, fontsize=7, ha="right")
plt.tight_layout()
fig = sns_plot.get_figure()
fig.savefig("country.png")
plt.title('Country')
plt.show()


hearmap_data = result.groupby(
    ['CountryName', 'Category']).size().reset_index(name='counts')
hearmap_data = hearmap_data.sort_values(['counts'], ascending=False).head(30)
hm = hearmap_data.pivot_table(
    index='CountryName', columns='Category', values='counts')
plt.figure(figsize=(25, 30))
sns_plot = sns.heatmap(hm, cmap='coolwarm', linecolor='black')
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=30, fontsize=7, ha="right")
fig = sns_plot.get_figure()
fig.savefig("heatmap1.png")
plt.title('Pivot1')
plt.show()
