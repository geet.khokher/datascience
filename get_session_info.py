import csv
import datetime
import json

import mysql.connector
from mysql.connector import Error

cnx = mysql.connector.connect(host='localhost',
                              database='cja',
                              user='root',
                              password='root')
cursor = cnx.cursor()

query = ("SELECT Browser.BrowserInfo, Browser.LocationInfo, Browser.CreatedOn ,Browser.SessionKey FROM Browser")

cursor.execute(query)

with open('session_info_dataset.csv', 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(['Session', 'Browser', 'City', 'Region_code', 'region', 'Country', 'Continent',
                     'CountryName', 'Latitude', 'Longitude', 'DateYear', 'DateMonth'])
    for row in cursor.fetchmany(140000):
        browserName = json.loads(row[0])['browserPlatform']
        city = json.loads(row[1])['city'] if 'city' in json.loads(
            row[1]) else None
        region = json.loads(row[1])['region'] if 'region' in json.loads(
            row[1]) else None
        continent_code = json.loads(
            row[1])['continent_code'] if 'continent_code' in json.loads(row[1]) else None
        region_code = json.loads(
            row[1])['region_code'] if 'region_code' in json.loads(row[1]) else None
        country = json.loads(
            row[1])['country'] if 'country' in json.loads(row[1]) else None
        country_name = json.loads(
            row[1])['country_name'] if 'country_name' in json.loads(row[1]) else None
        latitude = json.loads(
            row[1])['latitude'] if 'latitude' in json.loads(row[1]) else None
        longitude = json.loads(
            row[1])['longitude'] if 'longitude' in json.loads(row[1]) else None
        date_year = row[2].year
        date_month = row[2].month
        sessionkey = row[3]
        writer.writerow([sessionkey, browserName, city, region_code, region, country, continent_code,
                         country_name, latitude, longitude, date_year, date_month])
cursor.close()
cnx.close()
