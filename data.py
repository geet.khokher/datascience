import os
import pdb

import matplotlib as mlp
import matplotlib.pyplot as plt
import numpy as np  # linear algebra
import pandas as pd  # data processing, CSV file I/O (e.g. pd.read_csv)
import plotly.graph_objs as go
import plotly.plotly as py
import seaborn as sns
from plotly.offline import download_plotlyjs, init_notebook_mode, iplot, plot

init_notebook_mode(connected=True)

df = pd.read_csv('dataset.csv')
df.isna().any()

df.fillna(value='', inplace=True)
plt.figure(figsize=(20, 15))
descending_order = df['CountryName'].value_counts(
).sort_values(ascending=False).index
sns_plot = sns.countplot(
    x=df['CountryName'], order=descending_order[:10], linewidth=4)
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=40, fontsize=7, ha="right")
plt.tight_layout()
fig = sns_plot.get_figure()
fig.savefig("country.png")
plt.title('Country')
plt.show()


plt.figure(figsize=(25, 30))
descending_order = df['PageTitle'].value_counts(
).sort_values(ascending=False).index
sns_plot = sns.countplot(
    x=df['PageTitle'], order=descending_order[:20], linewidth=4)
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=50, fontsize=5, ha="right")
fig = sns_plot.get_figure()
fig.savefig("Page.png")
plt.title('Page')
plt.show()

plt.figure(figsize=(25, 30))
df['Monthly'] = df.apply(lambda x: '%s_%s' %
                         (x['DateMonth'], x['DateYear']), axis=1)
descending_order = df['Country'].value_counts(
).sort_values(ascending=False).index
sns_plot = sns.countplot(x=df['Country'], hue=df['Monthly'],
                         order=descending_order[:20], linewidth=4)
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=50, fontsize=5, ha="right")
fig = sns_plot.get_figure()
fig.savefig("yearly.png")
plt.title('Yearly')
plt.show()

dataframe = df.groupby(df['CountryName']).size().reset_index(name='counts')
scl = [[0.0, 'rgb(242,240,247)'], [0.2, 'rgb(218,218,235)'], [0.4, 'rgb(188,189,220)'],
       [0.6, 'rgb(158,154,200)'], [0.8, 'rgb(117,107,177)'], [1.0, 'rgb(84,39,143)']]
data2 = dict(type='choropleth',
             colorscale=scl,
             autocolorscale=False,
             locations=dataframe['CountryName'],
             locationmode='country names',
             z=dataframe['counts'],
             text=dataframe['CountryName'],
             colorbar={'title': 'Visits'})
layout = dict(title='Netsolutions visits',
              geo=dict(showframe=False,
                       projection={'type': 'mercator'}))
choromap3 = go.Figure(data=[data2], layout=layout)
iplot(choromap3)
fig = dict(data=[data2], layout=layout)
url = plot(fig, filename='d3-world-map.html')
print(url)


hearmap_data = df.groupby(
    ['DateYear', 'DateMonth']).size().reset_index(name='counts')
hm = hearmap_data.pivot_table(
    index='DateYear', columns='DateMonth', values='counts')
plt.figure(figsize=(25, 30))
sns_plot = sns.heatmap(hm, cmap='coolwarm', linecolor='black')
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=30, fontsize=7, ha="right")
fig = sns_plot.get_figure()
fig.savefig("heatmap.png")
plt.title('Pivot')
plt.show()


hearmap_data = df.groupby(
    ['CountryName', 'DateMonth']).size().reset_index(name='counts')
hearmap_data = hearmap_data.sort_values(['counts'], ascending=False).head(20)
hm = hearmap_data.pivot_table(
    index='CountryName', columns='DateMonth', values='counts')
plt.figure(figsize=(25, 30))
sns_plot = sns.heatmap(hm, cmap='coolwarm', linecolor='black')
sns_plot.set_xticklabels(sns_plot.get_xticklabels(),
                         rotation=30, fontsize=7, ha="right")
fig = sns_plot.get_figure()
fig.savefig("heatmap1.png")
plt.title('Pivot1')
plt.show()
